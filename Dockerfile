FROM openjdk:17.0.2-oracle

ARG JAR_FILE=target/*.jar

WORKDIR /application
COPY $JAR_FILE ug-project.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "/application/ug-project.jar"]