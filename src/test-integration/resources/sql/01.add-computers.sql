insert into integration.computers(name, account_date, price_usd, price_pln)
values
('Toshiba', '2020-12-01', 1234.45, 56787.23),
('Lenovo', '2021-08-24', 45.23, 213.22),
('Macbook', '2022-01-30', 10.98, 89.345);