package org.dominik.server.configuration;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;

@TestConfiguration
@Import(JpaConfig.class)
@Profile("integration")
public class DataJpaTestConfig {
}
