package org.dominik.server.repositories;

import org.dominik.server.configuration.DataJpaTestConfig;
import org.dominik.server.entities.Computer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Import(DataJpaTestConfig.class)
@Sql("classpath:sql/01.add-computers.sql")
@ActiveProfiles("integration")
class ComputerRepositoryIT {
  @Autowired ComputerRepository repository;

  @Test
  @DisplayName("should save new computer")
  void shouldSaveNewComputer() {
    String name = "Toshiba";
    LocalDate accountDate = LocalDate.parse("2018-10-21");
    BigDecimal priceUSD = BigDecimal.valueOf(345.567);
    BigDecimal pricePLN = BigDecimal.valueOf(13234.56);

    Computer computer = new Computer(name, accountDate, priceUSD, pricePLN);
    var saved = repository.save(computer);

    assertNotNull(saved.getId());
    assertEquals(name, saved.getName());
    assertEquals(priceUSD, saved.getPriceUSD());
    assertEquals(pricePLN, saved.getPricePLN());
    assertEquals(accountDate, saved.getAccountDate());
    assertNotNull(saved.getCreatedAt());
    assertNotNull(saved.getUpdatedAt());
    assertEquals(saved.getCreatedAt(), saved.getUpdatedAt());
    assertEquals(0, saved.getVersion());
  }

  @Test
  @DisplayName("should find 3 computers")
  void shouldFind3Comps() {
    List<Computer> computers = repository.findAll();

    assertEquals(3, computers.size());
  }

  @Test
  @DisplayName("should return Toshiba if 'to' was passed")
  void shouldReturnToshibeIfToWasPassed() {
    List<Computer> computers = repository.findByNameContainingIgnoreCase("to");

    assertEquals(1, computers.size());
    assertEquals("Toshiba", computers.get(0).getName());
  }

  @Test
  @DisplayName("should return Lenovo if 'ov' was passed")
  void shouldReturnLenovoIfOvWasPassed() {
    List<Computer> computers = repository.findByNameContainingIgnoreCase("ov");

    assertEquals(1, computers.size());
    assertEquals("Lenovo", computers.get(0).getName());
  }

  @Test
  @DisplayName("should return an empty list if computer was not found")
  void shouldReturnAnEmptyListIfComputerWasNotFound() {
    List<Computer> computers = repository.findByNameContainingIgnoreCase("bl");
    assertEquals(0, computers.size());
  }

  @Test
  @DisplayName("should return Toshiba if account date is 2020-12-01")
  void shouldReturnToshibaIfAccountDateIs2020_12_01() {
    List<Computer> computers = repository.findByAccountDate(LocalDate.parse("2020-12-01"));

    assertEquals(1, computers.size());
    assertEquals("Toshiba", computers.get(0).getName());
  }

  @Test
  @DisplayName("should return Macbook if account date is 2022-01-30")
  void shouldReturnMacbookIfAccountDateIs2022_01_30() {
    List<Computer> computers = repository.findByAccountDate(LocalDate.parse("2022-01-30"));

    assertEquals(1, computers.size());
    assertEquals("Macbook", computers.get(0).getName());
  }
}
