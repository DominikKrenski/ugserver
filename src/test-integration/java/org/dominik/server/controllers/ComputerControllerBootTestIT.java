package org.dominik.server.controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.dominik.server.dto.ComputerDTO;
import org.dominik.server.services.definitions.ComputerService;
import org.dominik.server.services.definitions.FileService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;

import java.util.List;

import static org.hamcrest.Matchers.matchesPattern;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(
  webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
  properties = {
    "spring.profiles.active=integration"
  }
)
@AutoConfigureMockMvc
@Transactional
@Sql("classpath:sql/01.add-computers.sql")
@ActiveProfiles("integration")
class ComputerControllerBootTestIT {
  private static final String BASE_URL = "/computers";
  private static final String TIMESTAMP_PATTERN = "^\\d{2}-\\d{2}-\\d{4}T\\d{2}:\\d{2}:\\d{2}\\.\\d{3}Z$";

  @TestConfiguration
  static class SimpleConfig {
    @Bean
    public FileService fileService() {
      FileService fileService = mock(FileService.class);
      return fileService;
    }
  }

  @Autowired MockMvc mvc;
  @Autowired ObjectMapper mapper;
  @Autowired ComputerService computerService;
  @Autowired FileService fileService;

  @Test
  @DisplayName("should add new computer")
  void shouldAddNewComputer() throws Exception {
    doNothing().when(fileService).writeToFile(any(ComputerDTO.class));

    String json = """
      {
        "name": "Macbook Pro 2020",
        "accountDate": "10-08-2021",
        "priceUSD": "34.45",
        "pricePLN": "124.45"
      }
      """;

    mvc
      .perform(
        post(BASE_URL)
          .content(json)
          .contentType(MediaType.APPLICATION_JSON)
          .accept(MediaType.APPLICATION_JSON)
      )
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.name").value("Macbook Pro 2020"))
      .andExpect(jsonPath("$.accountDate").value("10-08-2021"))
      .andExpect(jsonPath("$.priceUSD").value("34.45"))
      .andExpect(jsonPath("$.pricePLN").value("124.45"))
      .andExpect(jsonPath("$.createdAt", matchesPattern(TIMESTAMP_PATTERN)))
      .andExpect(jsonPath("$.updatedAt", matchesPattern(TIMESTAMP_PATTERN)));
  }

  @Test
  @DisplayName("should return 3 computers")
  void shouldReturn3Computers() throws Exception {
    mvc
      .perform(
        get(BASE_URL)
          .accept(MediaType.APPLICATION_JSON)
      )
      .andExpect(status().isOk())
      .andDo(res -> {
        String body = res.getResponse().getContentAsString();

        List<ComputerDTO> dtos = mapper.readValue(body, new TypeReference<List<ComputerDTO>>(){});

        assertEquals(3, dtos.size());
      });
  }

  @Test
  @DisplayName("should return Lenovo if query is 'no'")
  void shouldReturnLenovoIfQueryIsNo() throws Exception {
    mvc
      .perform(
        get(BASE_URL + "?name=no")
          .accept(MediaType.APPLICATION_JSON)
      )
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.[0].name").value("Lenovo"));
  }

  @Test
  @DisplayName("should return 3 computers if query is 'o'")
  void shouldReturn3ComputersIfQueryIsO() throws Exception {
    mvc
      .perform(
        get(BASE_URL + "?name=o")
          .accept(MediaType.APPLICATION_JSON)
      )
      .andExpect(status().isOk())
      .andDo(res -> {
        String body = res.getResponse().getContentAsString();
        List<ComputerDTO> dtos = mapper.readValue(body, new TypeReference<List<ComputerDTO>>(){});

        assertEquals(3, dtos.size());
      });
  }

  @Test
  @DisplayName("should return an empty list if computer with given name does not exist")
  void shouldReturnEmptyListIfComputerWithGivenNameDoesNotExist() throws Exception {
    mvc
      .perform(
        get(BASE_URL + "?name=Del")
          .accept(MediaType.APPLICATION_JSON)
      )
      .andExpect(status().isOk())
      .andDo(res -> {
        String body = res.getResponse().getContentAsString();
        List<ComputerDTO> dtos = mapper.readValue(body, new TypeReference<List<ComputerDTO>>(){});

        assertEquals(0, dtos.size());
      });
  }

  @Test
  @DisplayName("should return Macbook if searching by date")
  void shouldReturnMacbookIfSearchingByDate() throws Exception {
    mvc
      .perform(
        get(BASE_URL + "?accountDate=30-01-2022")
          .accept(MediaType.APPLICATION_JSON)
      )
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.[0].name").value("Macbook"));
  }
}
