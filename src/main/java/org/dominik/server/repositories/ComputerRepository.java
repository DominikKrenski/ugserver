package org.dominik.server.repositories;

import org.dominik.server.entities.Computer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface ComputerRepository extends JpaRepository<Computer, Long> {
  List<Computer> findByNameContainingIgnoreCase(String namePart);
  List<Computer> findByAccountDate(LocalDate accountDate);
}
