package org.dominik.server.errors.exceptions;

import lombok.NonNull;

public final class InternalServerException extends BaseException {

  public InternalServerException(@NonNull String message) {
    super(message);
  }
}
