package org.dominik.server.errors.exceptions;

import lombok.NonNull;
import lombok.ToString;
import org.springframework.http.HttpStatus;

@ToString(callSuper = true)
public final class BadRequestException extends BaseException {
  public BadRequestException(@NonNull String message) {
    super(HttpStatus.BAD_REQUEST, message);
  }
}
