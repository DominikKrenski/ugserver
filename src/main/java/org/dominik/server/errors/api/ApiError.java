package org.dominik.server.errors.api;

import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import org.springframework.http.HttpStatus;

import java.time.Instant;
import java.util.List;

@Getter
@ToString
public final class ApiError {
  private final String status;
  private final Instant timestamp;
  private final String message;
  private final List<SubError> errors;

  private ApiError(@NonNull String status, @NonNull Instant timestamp, @NonNull String message, List<SubError> errors) {
    this.status = status;
    this.timestamp = timestamp;
    this.message = message;
    this.errors = errors;
  }

  public static Builder builder() {
    return new Builder();
  }

  public static final class Builder {
    private String status;
    private Instant timestamp;
    private String message;
    private List<SubError> errors;

    private Builder() {}

    public Builder status(@NonNull HttpStatus status) {
      this.status = status.getReasonPhrase();
      return this;
    }

    public Builder timestamp(@NonNull Instant timestamp) {
      this.timestamp = timestamp;
      return this;
    }

    public Builder message(@NonNull String message) {
      this.message = message;
      return this;
    }

    public Builder errors(@NonNull List<SubError> errors) {
      this.errors = errors;
      return this;
    }

    public ApiError build() {
      return new ApiError(status, timestamp, message, errors);
    }
  }
}
