package org.dominik.server.advices;

import lombok.extern.slf4j.Slf4j;
import org.dominik.server.errors.api.ApiError;
import org.dominik.server.errors.api.ValidationError;
import org.dominik.server.errors.exceptions.BaseException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.Instant;
import java.util.*;

@Slf4j
@RestControllerAdvice
public class AppControllerAdvice extends ResponseEntityExceptionHandler {

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @Override
  protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
    log.error(Arrays.toString(ex.getStackTrace()));
    ApiError apiError = ApiError
      .builder()
      .status(HttpStatus.BAD_REQUEST)
      .timestamp(Instant.now())
      .message("Message is not formatted properly")
      .build();

    return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(BaseException.class)
  public ResponseEntity<Object> handleBaseException(BaseException exception) {
    ApiError apiError = ApiError.builder()
      .status(exception.getStatus())
      .timestamp(exception.getTimestamp())
      .message(exception.getMessage())
      .build();

    return new ResponseEntity<>(apiError, exception.getStatus());
  }

  private List<ValidationError> prepareValidationErrors(List<FieldError> fieldErrors) {
    Map<String, ValidationError> map = new HashMap<>();

    fieldErrors
      .forEach(fieldError -> {
        if (map.containsKey(fieldError.getField())) {
          map.get(fieldError.getField()).getValidationMessages().add(fieldError.getDefaultMessage());
        } else {
          List<String> messages = new LinkedList<>();
          messages.add(fieldError.getDefaultMessage());
          map.put(fieldError.getField(),
            new ValidationError(fieldError.getField(), fieldError.getRejectedValue(), messages));
        }
      });

    return new LinkedList<>(map.values());
  }
}
