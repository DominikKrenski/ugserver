package org.dominik.server.services.implementations;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NonNull;
import org.dominik.server.dto.ComputerDTO;
import org.dominik.server.dto.ComputerXML;
import org.dominik.server.dto.Facture;
import org.dominik.server.services.definitions.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class FileServiceImpl implements FileService {
  private static final String FILE_NAME = "facture.xml";

  private final XmlMapper mapper;

  @Autowired
  public FileServiceImpl(XmlMapper mapper) {
    this.mapper = mapper;
  }

  @Override
  public void writeToFile(@NonNull ComputerDTO computerDTO) throws IOException {
    ComputerXML computerXML = ComputerXML.from(computerDTO);

    Path path = Paths.get(FILE_NAME);

    Facture facture = null;

    if (Files.exists(path)) {
      facture = mapper.readValue(path.toFile(), Facture.class);
    } else {
      facture = new Facture();
    }

    facture.getComputers().add(computerXML);

    mapper.writeValue(path.toFile(), facture);
  }
}
