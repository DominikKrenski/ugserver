package org.dominik.server.services.implementations;

import lombok.NonNull;
import org.dominik.server.dto.ComputerDTO;
import org.dominik.server.entities.Computer;
import org.dominik.server.errors.exceptions.NotFoundException;
import org.dominik.server.repositories.ComputerRepository;
import org.dominik.server.services.definitions.ComputerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Service
public class ComputerServiceImpl implements ComputerService {
  private final ComputerRepository computerRepository;

  @Autowired
  public ComputerServiceImpl(ComputerRepository computerRepository) {
    this.computerRepository = computerRepository;
  }

  @Override
  public ComputerDTO save(@NonNull String name, @NonNull LocalDate accountDate, @NonNull BigDecimal priceUSD, @NonNull BigDecimal pricePLN) {
    return ComputerDTO.from(computerRepository.save(new Computer(name, accountDate, priceUSD, pricePLN)));
  }

  @Override
  public List<ComputerDTO> findAllComputers() {
    List<Computer> computers = computerRepository.findAll();

    return computers
      .stream()
      .map(ComputerDTO::from)
      .toList();
  }

  @Override
  public List<ComputerDTO> findComputerByName(@NonNull String name) {
    return computerRepository
      .findByNameContainingIgnoreCase(name)
      .stream()
      .map(ComputerDTO::from)
      .toList();
  }

  @Override
  public List<ComputerDTO> findComputerByAccountDate(@NonNull LocalDate accountDate) {
    return computerRepository
      .findByAccountDate(accountDate)
      .stream()
      .map(ComputerDTO::from)
      .toList();
  }
}
