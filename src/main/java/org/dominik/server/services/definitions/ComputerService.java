package org.dominik.server.services.definitions;

import org.dominik.server.dto.ComputerDTO;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public interface ComputerService {
  ComputerDTO save(String name, LocalDate accountDate, BigDecimal priceUSD, BigDecimal pricePLN);
  List<ComputerDTO> findAllComputers();
  List<ComputerDTO> findComputerByName(String name);
  List<ComputerDTO> findComputerByAccountDate(LocalDate accountDate);
}
