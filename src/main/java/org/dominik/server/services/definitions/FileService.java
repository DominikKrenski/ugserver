package org.dominik.server.services.definitions;

import org.dominik.server.dto.ComputerDTO;

import java.io.IOException;

public interface FileService {
  void writeToFile(ComputerDTO computerDTO) throws IOException;
}
