package org.dominik.server.utils.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public final class LocalDateSerializer extends JsonSerializer<LocalDate> {
  private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");

  @Override
  public void serialize(LocalDate localDate, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
    String date = dtf.format(localDate);
    jsonGenerator.writeString(date);
  }
}
