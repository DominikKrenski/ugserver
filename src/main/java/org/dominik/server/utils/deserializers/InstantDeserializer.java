package org.dominik.server.utils.deserializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

public class InstantDeserializer extends StdDeserializer<Instant> {
  private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("UTC"));

  public InstantDeserializer() {
    this(null);
  }

  public InstantDeserializer(Class<?> vc) {super(vc);}

  @Override
  public Instant deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
    String date = jsonParser.getText();
    TemporalAccessor temporalAccessor = dtf.parse(date);
    return Instant.from(temporalAccessor);
  }
}
