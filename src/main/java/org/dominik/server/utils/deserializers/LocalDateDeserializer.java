package org.dominik.server.utils.deserializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

public final class LocalDateDeserializer extends StdDeserializer<LocalDate> {
  private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");

  public LocalDateDeserializer() {
    this(null);
  }

  public LocalDateDeserializer(Class<?> vc) {
    super(vc);
  }

  @Override
  public LocalDate deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
    String date = jsonParser.getText();

    TemporalAccessor temporalAccessor = dtf.parse(date);
    return LocalDate.from(temporalAccessor);
  }
}
