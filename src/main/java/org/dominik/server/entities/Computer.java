package org.dominik.server.entities;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import java.io.Serial;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "computers")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public final class Computer extends Base implements Serializable {
  @Serial private static final long serialVersionUID = 1L;

  @Id
  @SequenceGenerator(
    name = "computers_seq_gen",
    sequenceName = "computers_id_seq",
    allocationSize = 1
  )
  @GeneratedValue(
    strategy = GenerationType.SEQUENCE,
    generator = "computers_seq_gen"
  )
  @Column(name = "id", nullable = false, updatable = false)
  private Long id;

  @Column(name = "name", length = 200, nullable = false)
  private String name;

  @Column(name = "account_date", columnDefinition = "DATE NOT NULL")
  private LocalDate accountDate;

  @Column(name = "price_usd", columnDefinition = "DECIMAL(10, 3) NOT NULL")
  private BigDecimal priceUSD;

  @Column(name = "price_pln", columnDefinition = "DECIMAL(10, 3) NOT NULL")
  private BigDecimal pricePLN;

  public Computer(@NonNull String name, @NonNull LocalDate accountDate, @NonNull BigDecimal priceUSD, @NonNull BigDecimal pricePLN) {
    this.name = name;
    this.accountDate = accountDate;
    this.priceUSD = priceUSD;
    this.pricePLN = pricePLN;
  }
}
