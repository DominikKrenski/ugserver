package org.dominik.server.configuration;

import org.dominik.server.repositories.ComputerRepository;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaAuditing
@EnableJpaRepositories(basePackageClasses = ComputerRepository.class)
public class JpaConfig {
}
