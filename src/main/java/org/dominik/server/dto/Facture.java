package org.dominik.server.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.LinkedList;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@JacksonXmlRootElement(localName = "faktura")
public class Facture {
  @JacksonXmlProperty(localName = "komputer")
  @JacksonXmlElementWrapper(useWrapping = false)
  List<ComputerXML> computers = new LinkedList<>();
}
