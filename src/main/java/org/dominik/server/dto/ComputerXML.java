package org.dominik.server.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.math.BigDecimal;
import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public final class ComputerXML {
  @JacksonXmlProperty(localName = "nazwa")
  private String name;

  @JacksonXmlProperty(localName = "data_ksiegowania")
  private LocalDate accountDate;

  @JacksonXmlProperty(localName = "koszt_USD")
  private BigDecimal priceUSD;

  @JacksonXmlProperty(localName = "koszt_PLN")
  private BigDecimal pricePLN;

  public static ComputerXML from(@NonNull ComputerDTO computerDTO) {
    return new ComputerXML(
      computerDTO.getName(),
      computerDTO.getAccountDate(),
      computerDTO.getPriceUSD(),
      computerDTO.getPricePLN()
    );
  }
}
