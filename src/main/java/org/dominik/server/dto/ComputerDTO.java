package org.dominik.server.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.dominik.server.entities.Computer;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Getter
@ToString
public final class ComputerDTO {
  @NonNull @EqualsAndHashCode.Include private Long id;
  @NonNull @EqualsAndHashCode.Include private String name;
  @NonNull private LocalDate accountDate;
  @NonNull private BigDecimal priceUSD;
  @NonNull private BigDecimal pricePLN;
  @NonNull private Instant createdAt;
  @NonNull private Instant updatedAt;
  @JsonIgnore private short version;

  public static ComputerDTO from(@NonNull Computer computer) {
    return new ComputerDTO(
      computer.getId(),
      computer.getName(),
      computer.getAccountDate(),
      computer.getPriceUSD(),
      computer.getPricePLN(),
      computer.getCreatedAt(),
      computer.getUpdatedAt(),
      computer.getVersion()
    );
  }
}
