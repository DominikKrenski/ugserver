package org.dominik.server.controllers;

import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.dominik.server.dto.ComputerDTO;
import org.dominik.server.errors.exceptions.BadRequestException;
import org.dominik.server.errors.exceptions.InternalServerException;
import org.dominik.server.services.definitions.ComputerService;
import org.dominik.server.services.definitions.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalAccessor;
import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/computers", produces = MediaType.APPLICATION_JSON_VALUE)
public class ComputerController {
  private final ComputerService computerService;
  private final FileService fileService;

  @Autowired
  public ComputerController(ComputerService computerService, FileService fileService) {
    this.computerService = computerService;
    this.fileService = fileService;
  }

  @GetMapping("")
  public List<ComputerDTO> findComputers(@RequestParam(required = false) String name, @RequestParam(required = false) String accountDate) {
    try {
      if (name != null) {
        return computerService.findComputerByName(name);
      } else if (accountDate != null) {
        LocalDate date = parseDate(accountDate);
        return computerService.findComputerByAccountDate(date);
      } else {
        return computerService.findAllComputers();
      }
    } catch (DateTimeParseException ex) {
      throw new BadRequestException("Account date is not formatted properly");
    }
  }

  @PostMapping("")
  public ComputerDTO saveComputer(@Valid @RequestBody Input input) {
    log.debug("INPUT: " + input);

    ComputerDTO computerDTO = computerService.save(input.getName(), input.getAccountDate(), input.getPriceUSD(), input.getPricePLN());

    try {
      fileService.writeToFile(computerDTO);
      return computerDTO;
    } catch (IOException ex) {
      throw new InternalServerException("There is problem with file generation");
    }
  }

  private LocalDate parseDate(String date) {
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    TemporalAccessor accessor = dtf.parse(date);
    return LocalDate.from(accessor);
  }

  @Getter
  @ToString
  public static final class Input {
    String name;
    LocalDate accountDate;
    BigDecimal priceUSD;
    BigDecimal pricePLN;
  }
}
