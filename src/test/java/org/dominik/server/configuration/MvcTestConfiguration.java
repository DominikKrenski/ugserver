package org.dominik.server.configuration;

import org.dominik.server.services.definitions.ComputerService;
import org.dominik.server.services.definitions.FileService;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;

@TestConfiguration
@Import(JacksonConfig.class)
public class MvcTestConfiguration {
  @MockBean ComputerService computerService;
  @MockBean FileService fileService;
}
