package org.dominik.server.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import com.jayway.jsonpath.ReadContext;
import org.dominik.server.entities.Computer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.regex.Pattern;

import static org.dominik.server.utils.TestUtils.createComputerInstance;
import static org.dominik.server.utils.TestUtils.createObjectMapperInstance;
import static org.junit.jupiter.api.Assertions.*;

class ComputerDtoSerializationTest {
  private static final String DATE_PATTERN = "^\\d{2}-\\d{2}-\\d{4}$";
  private static final String TIMESTAMP_PATTERN = "^\\d{2}-\\d{2}-\\d{4}T\\d{2}:\\d{2}:\\d{2}\\.\\d{3}Z$";
  private static ObjectMapper mapper;

  @BeforeAll
  private static void setUp() {
    mapper = createObjectMapperInstance();
  }

  @Test
  @DisplayName("should serialize ComputerDTO properly")
  void shouldSerializeComputerDtoProperly() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException, JsonProcessingException {
    Long id = 23L;
    String name = "Toshiba Inc.";
    LocalDate accountDate = LocalDate.now();
    BigDecimal priceUSD = BigDecimal.valueOf(23.34);
    BigDecimal pricePLN = priceUSD.multiply(BigDecimal.valueOf(4));
    Instant createdAt = Instant.now().minusSeconds(34565);
    Instant updatedAt = createdAt.plusSeconds(45544);
    short version = 23;

    Computer computer = createComputerInstance(id, name, accountDate, priceUSD, pricePLN, createdAt, updatedAt, version);
    var dto = ComputerDTO.from(computer);

    String json = mapper.writeValueAsString(dto);

    ReadContext ctx = JsonPath.parse(json);

    int jsonId = ctx.read("$.id");
    double jsonPriceUSD = ctx.read("$.priceUSD");
    double jsonPricePLN = ctx.read("$.pricePLN");

    assertEquals(id, jsonId);
    assertEquals(name, ctx.read("$.name"));
    assertTrue(Pattern.matches(DATE_PATTERN, ctx.read("$.accountDate")));
    assertEquals(priceUSD, BigDecimal.valueOf(jsonPriceUSD));
    assertEquals(pricePLN, BigDecimal.valueOf(jsonPricePLN));
    assertTrue(Pattern.matches(TIMESTAMP_PATTERN, ctx.read("$.createdAt")));
    assertTrue(Pattern.matches(TIMESTAMP_PATTERN, ctx.read("$.updatedAt")));
    assertThrows(PathNotFoundException.class, () -> ctx.read("$.version"));
  }
}
