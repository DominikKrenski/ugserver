package org.dominik.server.dto;

import org.dominik.server.entities.Computer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import static org.dominik.server.utils.TestUtils.createComputerInstance;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ComputerDtoCreationTest {

  @Test
  @DisplayName("should create ComputerDTO instance from Computer")
  void shouldCreateDtoInstanceFromDao() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
    Long id = 2L;
    String name = "Lenovo";
    LocalDate accountDate = LocalDate.parse("2021-02-01");
    BigDecimal priceUSD = BigDecimal.valueOf(234.34);
    BigDecimal pricePLN = priceUSD.multiply(BigDecimal.valueOf(4));
    Instant createdAt = Instant.now().minusSeconds(34556);
    Instant updatedAt = Instant.now().minusSeconds(1234);
    short version = 0;

    Computer computer = createComputerInstance(id, name, accountDate, priceUSD, pricePLN, createdAt, updatedAt, version);
    ComputerDTO dto = ComputerDTO.from(computer);

    assertEquals(computer.getId(), dto.getId());
    assertEquals(computer.getName(), dto.getName());
    assertEquals(computer.getAccountDate(), dto.getAccountDate());
    assertEquals(computer.getPriceUSD(), dto.getPriceUSD());
    assertEquals(computer.getPricePLN(), dto.getPricePLN());
    assertEquals(computer.getCreatedAt(), dto.getCreatedAt());
    assertEquals(computer.getUpdatedAt(), dto.getUpdatedAt());
    assertEquals(computer.getVersion(), dto.getVersion());
  }

  @ParameterizedTest
  @MethodSource("prepareDtoSet")
  @DisplayName("should throw NullPointerException if one of the properties is null")
  void shouldThrowNullPointerException(Computer computer) {
    assertThrows(NullPointerException.class, () -> ComputerDTO.from(computer));
  }

  private static List<Computer> prepareDtoSet() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
    List<Computer> computers = new LinkedList<>();
    computers.add(createComputerInstance(
      null, "a", LocalDate.now(), BigDecimal.valueOf(1), BigDecimal.valueOf(2), Instant.now(), Instant.now(), (short) 0)
    );

    computers.add(createComputerInstance(
      1L, null, LocalDate.now(), BigDecimal.valueOf(1), BigDecimal.valueOf(2), Instant.now(), Instant.now(), (short) 0
    ));

    computers.add(createComputerInstance(
      1L, "b", null, BigDecimal.ONE, BigDecimal.ONE, Instant.now(), Instant.now(), (short) 0
    ));

    computers.add(createComputerInstance(
      1L, "b", LocalDate.now(), null, BigDecimal.ONE, Instant.now(), Instant.now(), (short) 1
    ));

    computers.add(createComputerInstance(
      1L, "c", LocalDate.now(), BigDecimal.ONE, null, Instant.now(), Instant.now(), (short) 0
    ));

    computers.add(createComputerInstance(
      1L, "c", LocalDate.now(), BigDecimal.ONE, BigDecimal.ONE, null, Instant.now(), (short) 0
    ));

    computers.add(createComputerInstance(
      1L, "c", LocalDate.now(), BigDecimal.ONE, BigDecimal.ONE, Instant.now(), null, (short) 0
    ));

    return computers;
  }
}
