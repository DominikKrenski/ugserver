package org.dominik.server.utils.deserializers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.ToString;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.dominik.server.utils.TestUtils.createObjectMapperInstance;
import static org.junit.jupiter.api.Assertions.assertEquals;

class LocalDateDeserializationTest {
  private static ObjectMapper mapper;

  @BeforeAll
  private static void setUp() {
    mapper = createObjectMapperInstance();
  }

  @Test
  @DisplayName("should deserialize ComputerDTO instance")
  void shouldDeserializeComputerDtoInstance() throws JsonProcessingException {
    String json = """
      {
        "date": "15-02-2022"
      }
      """;

    Data data = mapper.readValue(json, Data.class);
    LocalDate date = LocalDate.of(2022, 2, 15);

    assertEquals(date, data.getDate());
  }

  @Getter
  @ToString
  private static final class Data {
    LocalDate date;
  }
}
