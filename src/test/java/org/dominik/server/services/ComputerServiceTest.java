package org.dominik.server.services;

import org.dominik.server.dto.ComputerDTO;
import org.dominik.server.entities.Computer;
import org.dominik.server.repositories.ComputerRepository;
import org.dominik.server.services.implementations.ComputerServiceImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.dominik.server.utils.TestUtils.createComputerInstance;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ComputerServiceTest {
  @Mock private ComputerRepository computerRepository;
  @InjectMocks private ComputerServiceImpl computerService;


  @Test
  @DisplayName("should return 2 ComputerDTO instances")
  void shouldReturn2ComputerDtoInstances() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
    List<Computer> computers = new ArrayList<>(2);

    computers.add(createComputerInstance(1L, "Toshiba", LocalDate.of(2021, 11, 15), BigDecimal.valueOf(34), BigDecimal.valueOf(456), Instant.now(), Instant.now(), (short) 0));
    computers.add(createComputerInstance(2L, "Lenovo", LocalDate.of(2022, 1, 23), BigDecimal.valueOf(345.56), BigDecimal.valueOf(23), Instant.now (), Instant.now(), (short) 1));

    when(computerRepository.findAll()).thenReturn(computers);

    List<ComputerDTO> dtos = computerService.findAllComputers();

    assertEquals(2, dtos.size());
  }

  @Test
  @DisplayName("should return an empty list if table is empty")
  void shouldReturnAnEmptyListIfTableIsEmpty() {
    when(computerRepository.findAll()).thenReturn(Collections.emptyList());

    List<ComputerDTO> dtos = computerService.findAllComputers();

    assertEquals(0, dtos.size());
  }

  @Test
  @DisplayName("should return Mackbook if ck was send")
  void shouldReturnMacbookIfCkWasSend() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
    Computer computer = createComputerInstance(2L, "Macbook", LocalDate.parse("2012-12-09"), BigDecimal.valueOf(345.34), BigDecimal.valueOf(123.45), Instant.now(), Instant.now(), (short) 1);

    when(computerRepository.findByNameContainingIgnoreCase("Ck")).thenReturn(List.of(computer));
    List<ComputerDTO> dtos = computerService.findComputerByName("Ck");

    assertEquals(1, dtos.size());
  }

  @Test
  @DisplayName("should return an empty list if computer with given name does not exist")
  void shouldThrowNotFoundIfComputerWithNameNotExist() {
    when(computerRepository.findByNameContainingIgnoreCase(anyString())).thenReturn(Collections.emptyList());

    List<ComputerDTO> dtos = computerService.findComputerByName("dklafj");

    assertEquals(0, dtos.size());
  }

  @Test
  @DisplayName("should return Toshiba if computer has been searched by account date")
  void shouldReturnToshibeIfComputerHasBeenSearchedByAccountData() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
    Computer computer = createComputerInstance(2L, "Toshiba", LocalDate.parse("2012-12-09"), BigDecimal.valueOf(345.34), BigDecimal.valueOf(123.45), Instant.now(), Instant.now(), (short) 1);

    when(computerRepository.findByAccountDate(any(LocalDate.class))).thenReturn(List.of(computer));

    List<ComputerDTO> dtos = computerService.findComputerByAccountDate(LocalDate.parse("2020-12-13"));
    assertEquals(1, dtos.size());
  }

  @Test
  @DisplayName("should return an empty list if computer with given account date does not exist")
  void shouldReturnAnEmptyListIfComputerWithGivenAccountDateDoesNotExist() {
    when(computerRepository.findByAccountDate(any(LocalDate.class))).thenReturn(Collections.emptyList());

    List<ComputerDTO> dtos = computerService.findComputerByAccountDate(LocalDate.parse("2020-09-09"));

    assertEquals(0, dtos.size());
  }

  @Test
  @DisplayName("should save new computer")
  void shouldSaveNewComputer() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
    Computer computer = createComputerInstance(2L, "Toshiba", LocalDate.parse("2012-12-09"), BigDecimal.valueOf(345.34), BigDecimal.valueOf(123.45), Instant.now(), Instant.now(), (short) 1);

    when(computerRepository.save(any(Computer.class))).thenReturn(computer);

    ComputerDTO dto = computerService.save("da", LocalDate.parse("2122-12-30"), BigDecimal.valueOf(34), BigDecimal.valueOf(345));

    assertEquals("Toshiba", dto.getName());
  }
}
