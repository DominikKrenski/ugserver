package org.dominik.server.controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.dominik.server.configuration.MvcTestConfiguration;
import org.dominik.server.dto.ComputerDTO;
import org.dominik.server.entities.Computer;
import org.dominik.server.services.definitions.ComputerService;
import org.dominik.server.services.definitions.FileService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.dominik.server.utils.TestUtils.createComputerInstance;
import static org.hamcrest.Matchers.matchesPattern;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(
  value = ComputerController.class,
  properties = {
    "spring.main.banner-mode=off"
  }
)
@Import(MvcTestConfiguration.class)
@ActiveProfiles("test")
class ComputerControllerMvcTest {
  private static final String BASE_URL = "/computers";
  private static final String TIMESTAMP_PATTERN = "^\\d{2}-\\d{2}-\\d{4}T\\d{2}:\\d{2}:\\d{2}\\.\\d{3}Z$";

  @Autowired MockMvc mvc;
  @Autowired ObjectMapper mapper;
  @Autowired ComputerService computerService;
  @Autowired FileService fileService;

  @Test
  @DisplayName("should save new computer")
  void shouldSaveNewComputer() throws Exception {
    String input = """
      {
        "name": "Toshiba Inc.",
        "accountDate": "01-12-2010",
        "priceUSD": 23.34,
        "pricePLN": 96.78
      }
      """;

    Instant createdAt = Instant.now();
    Computer computer = createComputerInstance(1L, "Toshiba Inc.", LocalDate.parse("2010-12-01"), BigDecimal.valueOf(23.34), BigDecimal.valueOf(96.78), createdAt, createdAt, (short) 0);

    when(computerService.save(anyString(), any(LocalDate.class), any(BigDecimal.class), any(BigDecimal.class))).thenReturn(ComputerDTO.from(computer));
    doNothing().when(fileService).writeToFile(any(ComputerDTO.class));

    mvc
      .perform(
        post(BASE_URL)
          .content(input)
          .contentType(MediaType.APPLICATION_JSON)
          .accept(MediaType.APPLICATION_JSON)
      )
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.name").value("Toshiba Inc."))
      .andExpect(jsonPath("$.accountDate").value("01-12-2010"))
      .andExpect(jsonPath("$.priceUSD").value("23.34"))
      .andExpect(jsonPath("$.pricePLN").value("96.78"))
      .andExpect(jsonPath("$.createdAt", matchesPattern(TIMESTAMP_PATTERN)));
  }

  @Test
  @DisplayName("should return BadRequest if date is not formatted properly")
  void shouldReturnBadRequestIfDateIsNotFormattedProperly() throws Exception {
    String input = """
      {
        "name": "Toshiba Inc.",
        "accountDate": "2010-12-09",
        "priceUSD": 23.34,
        "pricePLN": 96.78
      }
      """;

    Instant createdAt = Instant.now();
    Computer computer = createComputerInstance(1L, "Toshiba Inc.", LocalDate.parse("2010-12-01"), BigDecimal.valueOf(23.34), BigDecimal.valueOf(96.78), createdAt, createdAt, (short) 0);

    mvc
      .perform(
        post(BASE_URL)
          .content(input)
          .contentType(MediaType.APPLICATION_JSON)
          .accept(MediaType.APPLICATION_JSON)
      )
      .andExpect(status().isBadRequest())
      .andExpect(jsonPath("$.message").value("Message is not formatted properly"))
      .andExpect(jsonPath("$.timestamp", matchesPattern(TIMESTAMP_PATTERN)))
      .andExpect(jsonPath("$.status").value(HttpStatus.BAD_REQUEST.getReasonPhrase()));
  }

  @Test
  @DisplayName("should return an empty list if there is no computers")
  void shouldReturnEmptyListIfThereIsNoComputers() throws Exception {
    when(computerService.findAllComputers()).thenReturn(Collections.emptyList());

    mvc
      .perform(
        get(BASE_URL)
          .accept(MediaType.APPLICATION_JSON)
      )
      .andExpect(status().isOk())
      .andDo(res -> {
        String body = res.getResponse().getContentAsString();

        List<ComputerDTO> dtos = mapper.readValue(body, new TypeReference<List<ComputerDTO>>(){});

        assertEquals(0, dtos.size());
      });
  }

  @Test
  @DisplayName("should return two computers")
  void shouldReturn2Computers() throws Exception {
    Instant createdAt = Instant.now();
    Computer computer1 = createComputerInstance(1L, "Toshiba Inc.", LocalDate.parse("2010-12-01"), BigDecimal.valueOf(23.34), BigDecimal.valueOf(96.78), createdAt, createdAt, (short) 0);
    Computer computer2 = createComputerInstance(1L, "Lenovo", LocalDate.parse("2020-10-19"), BigDecimal.valueOf(23.34), BigDecimal.valueOf(96.78), createdAt, createdAt, (short) 0);

    List<ComputerDTO> computers = new ArrayList<>(2);
    computers.addAll(List.of(ComputerDTO.from(computer1), ComputerDTO.from(computer2)));

    when(computerService.findAllComputers()).thenReturn(computers);

    mvc
      .perform(
        get(BASE_URL)
          .accept(MediaType.APPLICATION_JSON)
      )
      .andExpect(status().isOk())
      .andDo(res -> {
        String body = res.getResponse().getContentAsString();
        List<ComputerDTO> dtos = mapper.readValue(body, new TypeReference<List<ComputerDTO>>(){});

        assertEquals(2, dtos.size());
      });
  }
}
